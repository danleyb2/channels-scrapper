symbols_dict = {}
numbers = ''

for number in range(10):
    numbers += str(number + 1)
    symbols_dict[number + 1] = numbers
    
def good_or_bad_caption(caption, number_of_videos_in_post, symbols_dict):

    '''A function to decide if the caption is good or bad
    '''
    line_start = ''
    lines_set = {'1', '2', '3', '4', '5', '6', '7', '8', '9', }


    for line in caption.splitlines():
        count = 0
        if len(line) > 0:
            first_letter = line[0]
            first_2_letters = line[:2]
            
            if first_2_letters == '11':
                continue

            elif first_2_letters == '10':
                line_start += first_2_letters
                count += 1

            elif first_letter in lines_set:
                line_start += first_letter
                count += 1

            if count == number_of_videos_in_post:
                break

    if symbols_dict[number_of_videos_in_post] == line_start:
        
        return True # 'good caption, you can now download the videos'
    
    else:
        return False # 'bad caption, dont download this post'
    
    
# print(good_or_bad_caption('random_caption', 5, symbols_dict))