import sys
from setuptools import setup, find_packages

requires = [
    'requests>=2.18.4',
    'tqdm>=3.8.0'
]

if sys.version_info < (3, 2):
    requires.append('futures==2.2')
    requires.append('configparser')

setup(
    name='channels-scraper',
    version='1.0.0',
    description=("channels-scraper is a command-line application written in Python"
                 " that scrapes and downloads an instagram channel\'s videos. Use responsibly."),
    url='https://gitlab.com/danleyb2/channels-scrapper',
    download_url='https://gitlab.com/danleyb2/channels-scrapper/-/archive/v1.0.0/channels-scrapper-v1.0.0.zip',
    author='Nyaundi Brian',
    author_email='danleyb2@gmail.com',
    license='Public domain',
    packages=find_packages(exclude=['tests']),
    install_requires=requires,
    entry_points={
        'console_scripts': ['channels-scraper=instagram_scraper.app:main'],
    },
    test_suite='nose.collector',
    zip_safe=False,
    keywords=['instagram', 'scraper', 'download', 'media', 'videos']
)
