# Instagram video download python

Andrew Pelton

## Description

I need a script than can look through instagram profiles and download certain videos based on some criteria that I have defined.  
The scraper needs to be multithreaded, change proxies and change browsers so it doesn't get shut down by instagram.   
Im looking for someone with instagram scraping and aws experience.  


I need to go through all the channels and download just the posts with multiple videos  
so not posts with pictures  
and not posts with only 1 video  
also the caption need to look like that caption_identifier code  
also please download the caption and download and make sure the program checks what has been downloaded   
already so it doesnt do it twice  
also I need to make run multiple instances at once  

Install
-------
To install channels-scraper:
```bash
$ pip install https://gitlab.com/danleyb2/channels-scrapper/-/archive/v1.0.0/channels-scrapper-v1.0.0.zip
```

Alternatively, you can clone the project and run the following command to install:
Make sure you cd into the *channels-scraper* folder before performing the command below.
```
$ python setup.py install
```


Usage
-----

To scrape a user's videos:
```bash
$ channels-scraper <username>              
```

*By default, downloaded media will be placed in `<current working directory>/<username>/<post_shortcode>`.*

To specify multiple users, pass a delimited list of users:
```bash
$ channels-scraper username1,username2,username3           
```

You can also supply a file containing a list of usernames:
```bash
$ channels-scraper -f ig_users.txt           
```

```
# ig_users.txt

https://www.instagram.com/gymtears
https://www.instagram.com/gym.health_center
https://www.instagram.com/bumpsmumstums

# and so on...

```

OPTIONS
-------

```
--help -h           Show help message and exit.

--filename    -f    Path to a file containing a list of users to scrape.

--destination -d    Specify the download destination. By default, media will 
                    be downloaded to <current working directory>/<username>.

--retain-username -n  Creates a username subdirectory when the destination flag is
                      set.

--latest            Scrape only new media since the last scrape. Uses the last modified
                    time of the latest media item in the destination directory to compare.

--latest-stamps     Specify a file to save the timestamps of latest media scraped by user.
                    This works similarly to `--latest` except the file specified by
                    `--latest-stamps` will store the last modified time instead of using 
                    timestamps of media items in the destination directory. 
                    This allows the destination directories to be emptied whilst 
                    still maintaining history.

--cookiejar         File in which to store cookies so that they can be reused between runs.

--quiet       -q    Be quiet while scraping.

--maximum     -m    Maximum number of items to scrape.

--media-metadata    Saves the media metadata associated with the user's posts to 
                    <destination>/<username>.json. Can be combined with --media-types none
                    to only fetch the metadata without downloading the media.

--include-location  Includes location metadata when saving media metadata. 
                    Implicitly includes --media-metadata.

--comments          Saves the comment metadata associated with the posts to 
                    <destination>/<username>.json. Implicitly includes --media-metadata.
                    
--interactive -i    Enables interactive login challenge solving. Has 2 modes: SMS and Email

--retry-forever     Retry download attempts endlessly when errors are received

--tag               Scrapes the specified hashtag for media.

--filter            Scrapes the specified hashtag within a user's media.

--location          Scrapes the specified instagram location-id for media.

--search-location   Search for a location by name. Useful for determining the location-id of 
                    a specific place.
                    
```

Develop
-------

Clone the repo and create a virtualenv 
```bash
$ virtualenv venv
$ source venv/bin/activate
$ python setup.py develop
```


